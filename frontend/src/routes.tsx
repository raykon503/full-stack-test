import {Routes as RouterRoutes, Route, Navigate} from 'react-router-dom';
import { LoginPage } from "./pages/login";
import Sign from "./pages/auth/sign";
import React, {useEffect, useState} from "react";
import DashboardPage from "./pages/painel";
import Users from "./pages/users";

const Routes: React.FC = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(() => {
        const storedValue = localStorage.getItem('token');
        return storedValue !== null || storedValue !== undefined || storedValue !== 'undefined' ? storedValue === 'true' : false;
    });

    useEffect(() => {
        const token = localStorage.getItem('token');

        if (token) {
            setIsLoggedIn(true);
        }

    }, [isLoggedIn]);

    return (
        <RouterRoutes>
            {!isLoggedIn ? (
                    <>
                        <Route path="/" element={<Navigate to="/login" />} />
                        <Route path="/login" element={<LoginPage />} />
                        <Route path="/signin" element={<Sign />} />
                        <Route path="*" element={<Navigate to="/login" />} />
                    </>
                ) : (
                    <>
                        <Route path="/" element={<Navigate to="/dashboard" />} />
                        <Route path="/dashboard" element={<DashboardPage isLoggedIn={isLoggedIn} />} />
                        <Route path="/users" element={<Users isLoggedIn={isLoggedIn} />} />
                        <Route path="*" element={<Navigate to="/dashboard" />} />
                    </>
                )
            }
        </RouterRoutes>
    );
};

export default Routes;
