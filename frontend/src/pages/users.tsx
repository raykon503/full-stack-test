import Loading from "../components/Loading";
import React, {useEffect, useState} from "react";
import {AuthLayout} from "./_layouts/auth";

const Users = ({ isLoggedIn }) => {
    const [loading, setLoading] = useState(false);

    const [listUsers, setListUsers] = useState([])
    const [idUserAuth] = useState(() => {
        return localStorage.getItem('idUser')
    })
    const todoUsers = async () => {
        try {
            setLoading(true)

            const response = await fetch(`http://127.0.0.1:8000/api/users/all/${idUserAuth}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
            });

            const data = await response.json();
            await setListUsers(data)

            setLoading(false);

        } catch (error) {
            console.log('Error:', error)
        }
    }

    useEffect(() => {
        todoUsers()
    }, [])

    const deleteUser = async (idUser) => {
        setLoading(true)

        try {
            const response =  await fetch(`http://127.0.0.1:8000/api/users/delete/${idUser}`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
            })

            const data = await response.json();

            if (data.status === 'ERROR') {
                console.log(data.message)
            }

            setLoading(false)

            //Atualizar a lista de users
            await todoUsers();

        } catch (error) {
            console.log('Error:', error)
        }
    }

    return (
        <>
            <Loading processing={loading}/>

            <AuthLayout isLoggedIn={isLoggedIn}>
                <div className="col-12">
                    <div className="card">
                        <div className="card-body">

                            {/* Renderização da lista de usuários */}
                            <div className="row">
                                <div className="table-responsive">
                                    <table className="table table-rounded table-row-bordered table-striped border table-row-gray-300 table-hover align-middle gs-10 gy-1">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Email</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {listUsers.length > 0 ? (
                                            listUsers.map((user) => (
                                                <tr key={user.id}>
                                                    <td>{user.name}</td>
                                                    <td>{user.email}</td>
                                                    <td>
                                                        <a onClick={() => deleteUser(user.id)}>
                                                            <i className="bi bi-trash-fill"
                                                               style={{
                                                                   color: 'red',
                                                                   fontSize: '20px'
                                                               }}></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                            <tr>
                                                <td colSpan="3">
                                                    Nenhum usuário na lista.
                                                </td>
                                            </tr>
                                        )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </AuthLayout>
        </>
    )
}

export default Users;