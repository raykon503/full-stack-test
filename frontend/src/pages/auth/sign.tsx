import { Link } from 'react-router-dom';
import React, {useState} from "react";
import {AuthLayout} from "../_layouts/auth";

const Sign: React.FC = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleRegister = async (e) => {
        e.preventDefault()

        if (name.trim() === '' || email.trim() === '' || password.trim() === '') {
            setError('Por favor, preencha todos os campos.');
            return;
        }

        try {
            const response = await fetch('http://127.0.0.1:8000/api/users/create', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ name, email, password }),
            });

            const data = await response.json();

            if (data.status === 'ERROR') {
                setError(data.message);
            }

            if (data.status === 'SUCCESS') {
                localStorage.setItem('token', data.token)
                localStorage.setItem('idUser', data.idUser)

                await window.location.reload();
            }

        } catch (error) {
            console.error('Error:', error);
            setError('Falha ao tentar criar um usuário. Por favor tente novamente mais tarde.');
        }
    };

    return (
        <AuthLayout>
            <div className="col-sm-6 col-md-4 align-self-center">
                <div className="card">
                    <div className="card-body">
                        {error && <div className="alert alert-danger">{error}</div>}

                        <form onSubmit={handleRegister}>
                            <div className="mb-3">
                                <label htmlFor="name" className="form-label">
                                    Name
                                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    name="name"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="email" className="form-label">
                                    Email address
                                </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="email"
                                    name="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="password" className="form-label">
                                    Password
                                </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="password"
                                    name="password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Register
                            </button>
                        </form>
                        <div className="mt-3">
                            Already have an account? <Link to="/login">Login</Link>
                        </div>
                    </div>
                </div>
            </div>
        </AuthLayout>
    );
}

export default Sign;