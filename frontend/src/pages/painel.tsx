import React, {useEffect, useState} from 'react';
import {AuthLayout} from "./_layouts/auth";
import Loading from "../components/Loading";
import "bootstrap-icons/font/bootstrap-icons.css"

const DashboardPage = ({ isLoggedIn }) => {
    const [showModal, setShowModal] = useState(false);
    const [loading, setLoading] = useState(false);

    const handleModalToggle = () => {
        setShowModal(!showModal);
    };

    /**
     * Parte de gerenciamento dos filmes
     */
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [genre, setGenre] = useState('');
    const [cover, setCover] = useState(null);

    const [error, setError] = useState('');

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        setCover(file);
    }

    const handleAddFilm = async (e) => {
        e.preventDefault()

        setLoading(true)

        if (title.trim() === '' || genre.trim() === '' || cover === null) {
            setError('Por favor, preencha todos os campos.');
            return;
        }

        const formData = new FormData();
        formData.append('title', title);
        formData.append('description', description);
        formData.append('genre', genre);
        formData.append('cover', cover);

        try {
            const response = await fetch('http://127.0.0.1:8000/api/films/create', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
                body: formData,
            });

            const data = await response.json();

            if (data.status === 'ERROR') {
                setError(data.message);
            }

            if (data.status === 'SUCCESS') {
                setShowModal(false)
            }

            setLoading(false)

            await window.location.reload();

        } catch (error) {
            console.log('Error:', error)
            setError('Falha ao tentar adicionar o filme. Por favor tente novamente mais tarde.');
        }
    }

    const [listFilms, setListFilms] = useState([])
    const [coverURLs, setCoverURLs] = useState({});
    const todoFilms = async () => {
        try {
            setLoading(true)

            const response = await fetch('http://127.0.0.1:8000/api/films/all', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
            });

            const data = await response.json();
            await setListFilms(data)

            const coverURLPromises = data.map(async (film) => {
                const coverURL = await imageFilm(film.cover_path);
                return { [film.id]: coverURL };
            });

            const coverURLResults = await Promise.all(coverURLPromises);
            const coverURLMap = Object.assign({}, ...coverURLResults);
            setCoverURLs(coverURLMap);

            setLoading(false);

        } catch (error) {
            console.log('Error:', error)
        }
    }

    useEffect(() => {
        todoFilms()
    }, [])

    const imageFilm = async (cover_path) => {
        setLoading(true)

        try {
            const response =  await fetch(`http://127.0.0.1:8000/api/storage/${cover_path}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
            })

            const blob = await response.blob();
            return URL.createObjectURL(blob);

            setLoading(false)

        } catch (error) {
            console.log('Error:', error)
        }
    }

    const deleteFilm = async (idFilm) => {
        setLoading(true)

        try {
            const response =  await fetch(`http://127.0.0.1:8000/api/films/delete/${idFilm}`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ESAbdf87uWpw96r2yYvL35gwjXAQdEA7QWXSCv7bjr20Hk85oE1GA1Ttard6LkZA`,
                },
            })

            const data = await response.json();

            if (data.status === 'ERROR') {
                console.log(data.message)
            }

            setLoading(false)

            await window.location.reload();

        } catch (error) {
            console.log('Error:', error)
        }
    }

    return (
        <>
            <Loading processing={loading}/>

            <AuthLayout isLoggedIn={isLoggedIn}>
            <div className="col-12">
                <div className="card">
                    <div className="card-body">
                        <div className="row mb-3">
                            <div className="col-2">
                                <button className="btn btn-primary" onClick={handleModalToggle}>
                                    Adicionar filme
                                </button>
                            </div>
                        </div>

                        {/* Renderização da lista de filmes */}
                        <div className="row">
                            <div className="table-responsive">
                                <table className="table table-rounded table-row-bordered table-striped border table-row-gray-300 table-hover align-middle gs-10 gy-1">
                                    <thead>
                                        <tr>
                                            <th>Titulo</th>
                                            <th>Gênero</th>
                                            <th>Capa do Filmes</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {listFilms.length > 0 ? (
                                            listFilms.map((film) => (
                                                <tr key={film.id}>
                                                    <td>{film.title}</td>
                                                    <td>{film.genre}</td>
                                                    <td>
                                                        {film.cover_path && (
                                                            <img
                                                                src={coverURLs[film.id]}
                                                                alt="Cover Image"
                                                                style={{ maxWidth: '100px' }}
                                                            />
                                                        )}
                                                    </td>
                                                    <td>
                                                        <a onClick={() => deleteFilm(film.id)}>
                                                            <i className="bi bi-trash-fill"
                                                               style={{
                                                                   color: 'red',
                                                                   fontSize: '20px'
                                                               }}></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                            <tr>
                                                <td colSpan="4">
                                                    Nenhum filme na sua lista.
                                                </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {/*Inicio do Modal*/}
                        <div className={`modal ${showModal ? 'show' : ''}`} tabIndex="-1" role="dialog" style={{ display: showModal ? 'block' : 'none' }}>
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title">Adicionar filme</h5>
                                        <button type="button" className="btn-close" onClick={handleModalToggle}></button>
                                    </div>
                                    <div className="modal-body">

                                        {error && <div className="alert alert-danger">{error}</div>}

                                        <div className="mb-3">
                                            <label htmlFor="title" className="form-label">
                                                Titulo
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="title"
                                                name="title"
                                                value={title}
                                                onChange={(e) => setTitle(e.target.value)}
                                                required
                                            />
                                        </div>

                                        <div className="mb-3">
                                            <label htmlFor="description" className="form-label">
                                                Descrição
                                            </label>
                                            <textarea
                                                className="form-control"
                                                id="description"
                                                name="description"
                                                value={description}
                                                onChange={(e) => setDescription(e.target.value)}
                                                rows="2"
                                            ></textarea>
                                        </div>

                                        <div className="mb-3">
                                            <label htmlFor="genre" className="form-label">
                                                Gênero
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="genre"
                                                name="genre"
                                                value={genre}
                                                onChange={(e) => setGenre(e.target.value)}
                                                required
                                            />
                                        </div>

                                        <div className="mb-3">
                                            <label htmlFor="cover" className="form-label">
                                                Capa do filme
                                            </label>
                                            <input
                                                type="file"
                                                className="form-control"
                                                id="cover"
                                                name="cover"
                                                accept="image/*"
                                                onChange={handleImageChange}
                                                required
                                            />

                                            {cover && (
                                                <div>
                                                    <img
                                                        src={URL.createObjectURL(cover)}
                                                        alt="Selected Image"
                                                        style={{ maxWidth: '100%', marginTop: '10px' }}
                                                    />
                                                </div>
                                            )}
                                        </div>

                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" onClick={handleModalToggle}>Fechar</button>

                                        <button type="button" className="btn btn-primary" onClick={(e) => handleAddFilm(e)}>Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Fim do modal */}
                    </div>
                </div>
            </div>
        </AuthLayout>
        </>
    );
};

export default DashboardPage;
