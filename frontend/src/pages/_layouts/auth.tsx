import React, {ReactNode} from 'react';
import Navbar from "../../components/navbar";
import 'bootstrap/dist/css/bootstrap.min.css';

interface AuthLayoutProps {
    children: ReactNode;
}

export function AuthLayout({ children, isLoggedIn }: AuthLayoutProps) {
    return (
        <div>
            <div className="w-100 mb-3">
                <Navbar isLoggedIn={isLoggedIn} />
            </div>

            <div className="container-fluid">
                <div className="row justify-content-center vh-100">
                    {children}
                </div>
            </div>
        </div>
    );
}
