import { Outlet } from 'react-router-dom';
import React from "react";

const AppLayout: React.FC = () => {

    return (
        <>
            <div className="container-fluid">
                <Outlet />
            </div>
        </>
    );
};

export default AppLayout;
