import {Link} from "react-router-dom"
import React from "react";

interface NavbarProps {
    isLoggedIn: boolean
}

const Navbar: React.FC<NavbarProps> = ({ isLoggedIn }) => {

    const handleLogout = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('idUser')

        window.location.reload();
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">
                    <img src="https://watchbr-resources-v3.s3.sa-east-1.amazonaws.com/assets/logos/logo-watch-3.svg" />
                </Link>
                <div className="collapse navbar-collapse justify-content-end">
                    <ul className="navbar-nav">
                        {isLoggedIn ? (
                            <>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/users">
                                        Usuários
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <a href="#" className="nav-link" onClick={handleLogout}>
                                        Sair
                                    </a>
                                </li>
                            </>
                        ) : (
                            <></>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;