import React from "react";
import Navbar from "./navbar";

const Loading: React.FC = ({ processing }) => {
    return(
        <div>
            {processing && (
                <div className="modal-backdrop show"
                     style={{
                         zIndex: 1070,
                         backgroundColor: 'rgba(0, 0, 0, 1)',
                         position: 'fixed',
                         top: 0,
                         left: 0,
                         right: 0,
                         bottom: 0,
                         display: 'flex',
                         justifyContent: 'center',
                         alignItems: 'center',
                     }}
                >
                    <div className="text-center text-white">
                        <div className="spinner-border" style={{width: '3rem', height: '3rem'}} role="status">
                            <span className="visually-hidden">Carregando Aguarde...</span>
                        </div>
                        <p style={{fontSize: '20px'}}>Carregando, aguarde...</p>
                    </div>
                </div>
            )}
        </div>
    )
}

export default Loading;