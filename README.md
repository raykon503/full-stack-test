# Instruções de Uso

Este repositório contém instruções para construir e executar o backend de uma aplicação, bem como para configurar um ambiente de banco de dados MySQL utilizando Docker.

## Configurando o Ambiente do Banco de Dados MySQL

### Docker Compose

O arquivo docker-compose.yaml está configurado para criar um ambiente de banco de dados MySQL.

Para iniciar o ambiente, execute o seguinte comando no terminal, estando na raiz do projeto:

```bash
docker-compose up -d
```

## Construindo e Executando o Backend

Entre na pasta backend e execute:
```bash
composer install
```

E logo após o término execute:
```bash
php artisan migrate
```
Para gerar o banco com suas tabelas para serem salvas

Então execute
```bash
php artisan serve
```
Para executar a aplicação do backend

## Construindo e Executando o Frontend

Em outro console executaremos os comandos para o frontend.
Entre na pasta frontend e execute:

```bash
npm install
```

E logo após o término execute:

```bash
npm run dev
```
