<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|string|email',
            'password'  => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => $validator->errors()], 401);
        }

        $user = User::where('email', $request->email)->first();

        $email = $request->input('email');
        $password = $request->input('password');

        if ($user && Auth::attempt(['email' => $email, 'password' => $password])) {
            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json(['status' => 'SUCCESS', 'token' => $token, 'idUser' => $user->id]);
        }

        return response()->json(['status' => 'ERROR', 'message' => 'Email e/ou senha incorretos'], 401);
    }
}
