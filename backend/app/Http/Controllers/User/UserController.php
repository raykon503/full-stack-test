<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Função que retorna todos os usuários
     * @return Collection
     */
    public function getUsers($iduser)
    {
        return User::where('id', '<>', $iduser)->get();
    }

    /**
     * Função que busca um usuário e retorna o mesmo
     * @param $iduser
     * @return mixed
     */
    public function getUser($iduser)
    {
        return User::findOrFail($iduser);
    }

    /**
     * Função que cria um usuário
     * @param Request $request
     * @return JsonResponse
     */
    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string',
            'email'     => 'required|string|email|max:255|unique:'.User::class,
            'password'  => ['required', Rules\Password::default()],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => $validator->errors()], 400);
        }

        $token = '';
        $user = '';

        try {
            DB::transaction(function () use ($request, &$token, &$user) {
                $user = User::create([
                        'email'     => $request->email,
                        'name'      => $request->name,
                        'password'  => $request->password
                ]);

                $token = $user->createToken('auth_token')->plainTextToken;
            });

            return response()->json(['status' => 'SUCCESS', 'token' => $token, 'idUser' => $user->id], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => 'ERROR', 'message' => $e->getMessage()], 401);
        }
    }

    /**
     * Função que edita o usuário
     * @param Request $request
     * @param $iduser
     * @return JsonResponse
     */
    public function updateUser(Request $request, $iduser)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string',
            'password'  => ['required', 'confirmed', Rules\Password::default()],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => $validator->errors()], 400);
        }

        //Verifica se existe caso contrário ele interrompe com fail no sistema
        $searchUser = $this->getUser($iduser);

        try {
            DB::transaction(function () use ($searchUser, $request) {
                $searchUser->name       = $request->name;
                $searchUser->password   = $request->password;
                $searchUser->save();
            });

            return response()->json(['status' => 'SUCCESS', 'message' => 'Usuário atualizado com sucesso.'], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => 'ERROR', 'message' => $e->getMessage()]. 401);
        }
    }

    /**
     * Função que deleta um usuário
     * @param $iduser
     * @return JsonResponse
     */
    public function deleteUser($iduser)
    {
        //Verifica se existe caso contrário ele interrompe com fail no sistema
        $searchUser = $this->getUser($iduser);

        try {
            DB::transaction(function () use ($searchUser) {
                $searchUser->delete();
            });

            return response()->json(['status' => 'SUCCESS', 'message' => 'Usuário deletado com sucesso.'], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => 'ERROR', 'message' => $e->getMessage()]. 401);
        }
    }
}
