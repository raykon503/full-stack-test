<?php

namespace App\Http\Controllers\Films;

use App\Http\Controllers\Controller;
use App\Models\Films;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller
{
    /**
     * Função que retorna todos os filmes
     * @return Collection
     */
    public function getFilms()
    {
        return Films::all();
    }

    /**
     * Função que busca um filme e retorna o mesmo
     * @param $idfilm
     * @return mixed
     */
    public function getFilm($idfilm)
    {
        return Films::findOrFail($idfilm);
    }

    /**
     * Função que cria um registro de filme
     * @param Request $request
     * @return JsonResponse
     */
    public function createFilm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'genre' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'message' => $validator->errors()], 400);
        }

        try {
            DB::transaction(function () use ($request) {
                $film = Films::create([
                    'title'         => $request->input('title'),
                    'description'   => $request->input('description')    ?? NULL,
                    'release_date'  => $request->input('release_date')   ?? NULL,
                    'duration'      => $request->input('duration')       ?? NULL,
                    'genre'         => $request->input('genre'),
                    'director'      => $request->input('director')       ?? NULL,
                    'cast'          => $request->input('cast')           ?? NULL,
                    'rating'        => $request->input('rating')         ?? NULL,
                ]);

                $filmId = $film->id;

                if (!$request->hasFile('cover')) {
                    return response()->json(['status' => 'ERROR', 'message' => 'Não foi possível identificar a imagem da capa do filme'], 400);
                }

                //Gerar um nome único para a imagem
                $nomeArquivo = 'cover_' . $filmId . '.' . $request->file('cover')->extension();

                //Salva o arquivo de imagem no diretório 'storage/app/covers'
                $coverPath = $request->file('cover')->storeAs('covers', $nomeArquivo);

                $film->cover_path = $coverPath;
                $film->save();
            });

            return response()->json(['status' => 'SUCCESS', 'message' => 'Filme publicado com sucesso.'], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => 'ERROR', 'message' => $e->getMessage()], 401);
        }
    }

    public function deleteFilm($idfilm)
    {
        $film = $this->getFilm($idfilm);

        try {
            DB::transaction(function () use ($film) {
                $filePath = storage_path('app/' . $film->cover_path);

                if (!File::exists($filePath)) {
                    return response()->json(['status' => 'ERROR', 'message' => 'Arquivo não encontrado'], 401);
                }

                File::delete($filePath);

                $film->delete();
            });

            return response()->json(['status' => 'SUCCESS', 'message' => 'Filme excluído com sucesso.'], 200);

        } catch (\Exception $e) {
            return response()->json(['status' => 'ERROR', 'message' => $e->getMessage()], 401);
        }
    }
}
