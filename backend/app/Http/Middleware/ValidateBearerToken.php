<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateBearerToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $bearerToken = $request->bearerToken();

        $validateToken = env('TOKEN');

        if($bearerToken !== $validateToken) {
            return response()->json(['status' => 'ERROR', 'message' => 'Bearer token inválido.'], 401);
        }

        return $next($request);
    }
}
