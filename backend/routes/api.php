<?php

use App\Http\Controllers\Films\FilmController;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\User\UserController;
use App\Http\Middleware\ValidateBearerToken;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/**
 * Rotas com verificação do bearer token no middleware
 */
Route::middleware([ValidateBearerToken::class])->group(function () {

    /**
     * Rotas dentro do grupo direto do controller a ser usado
     */
    Route::controller(UserController::class)->group(function () {
        Route::get('/users/all/{iduser}', 'getUsers')->name('getAllUsers')->where(['iduser' => '[0-9]+']);
        Route::get('/users/{iduser}', 'getUser')->name('getOneUser')->where(['iduser' => '[0-9]+']);
        Route::post('/users/create', 'createUser')->name('createUser');
        Route::post('/users/update/{iduser}', 'updateUser')->name('updateUser')->where(['iduser' => '[0-9]+']);
        Route::post('/users/delete/{iduser}', 'deleteUser')->name('deleteUser')->where(['iduser' => '[0-9]+']);
    });

    Route::controller(AuthController::class)->group(function () {
        Route::post('/login', 'login')->name('auth');
    });


    Route::controller(FilmController::class)->group(function () {
        Route::get('/films/all', 'getFilms')->name('getAllFilms');
        Route::get('/films/{idfilm}', 'getFilm')->name('getOneFilm')->where(['idfilm' => '[0-9]+']);
        Route::post('/films/create', 'createFilm')->name('createFilm');
        Route::post('/films/delete/{idfilm}', 'deleteFilm')->name('deleteFilm')->where(['idfilm' => '[0-9]+']);
    });

    Route::get('/storage/{filename}', function ($filename) {
        // Obtém o caminho do diretório de armazenamento
        $storagePath = storage_path('app');

        // Adiciona o nome do arquivo ao caminho do diretório de armazenamento
        $filePath = $storagePath . DIRECTORY_SEPARATOR . $filename;

        // Verifica se o arquivo existe
        if (!File::exists($filePath)) {
            abort(404);
        }

        // Retorna o arquivo como resposta
        return response()->file($filePath);
    })->where('filename', '.*');

});
